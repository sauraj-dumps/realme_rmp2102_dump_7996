## full_oppo8786-user 11 RP1A.200720.011 504 release-keys
- Manufacturer: realme
- Platform: mt6768
- Codename: RMP2102
- Brand: realme
- Flavor: full_oppo8786-user
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: 1650624509763
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: realme/RMP2102EEA/RE54C1L1:11/RP1A.200720.011/1650624509763:user/release-keys
realme/RMP2102RU/RE54C1L1:11/RP1A.200720.011/1650624509763:user/release-keys
realme/RMP2102/RE54C1L1:11/RP1A.200720.011/1650624509763:user/release-keys
realme/RMP2102TR/RE54C1L1:11/RP1A.200720.011/1650624509763:user/release-keys
- OTA version: 
- Branch: full_oppo8786-user-11-RP1A.200720.011-504-release-keys
- Repo: realme_rmp2102_dump_7996


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
